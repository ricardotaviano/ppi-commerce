<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->

<?php
require_once ("conexao.php");

conectar();
?>

<html>
   
    <?php 
    
    require 'head.php';
    
    ?>


    <body>

        <?php 
        
        require 'navbar.php';
        
        ?>
        
       
        

        <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel" >
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="img/car2.png" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="img/car1.png" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="img/car4.png" class="d-block w-100" alt="...">
                </div>
               
                <div class="carousel-item">
                    <img src="img/car3.jpg" class="d-block w-100" alt="...">
                </div>
            </div>
            
        </div>
        
        <br>
        <br>


        <br>
        <br>

        
        <div class="container ">
            
            
            <div class="row">
            
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 tela1">
                
                <h3 class="text-center" style="margin:20px" > Roteadores Ultra Velozes  </h3>
                
                <p class="text-center"> Conheça a linha de roteadores ultra velozes que vão turbinar
                a sua conexão.</p>

                
                
                
            </div>

                
             <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 tela2">
                
                <h4 class="text-center" style="margin:20px" > As melhores Antenas para mandar seu sinal mais longe e melhor!  </h4>
                

               
                
                
            </div>
            
            
             <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 tela3">
                
                <h3 class="text-center" style="margin:20px" > Precisa de Ajuda?  </h3>
                
                <p class="text-center"> Entre em contato com a gente, estamos prontos para
                    atende-los.</p>

          
                
                
            </div>
            
            </div>
  
        </div>
        
          
        <div class="container ">
            
            
            <div class="row">
            
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12 tela1">
                 
                <h3 class="text-center" style="margin:20px" > Roteadores Ultra Velozes  </h3>
                
                <p class="text-center"> Conheça a linha de roteadores ultra velozes que vão turbinar
                a sua conexão.</p>

                
                
                
            </div>

                
             <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12 telaimg1">
                

                 <h3 id="tel1" class="text-center align-middle" > Saiba como melhorar o sinal do seu WiFi com essas dicas incríveis.  </h3>
                
                
            </div>
            
            </div>
  
        </div>
        
        
        
        <?php require 'footer.php'; ?>
        
    </body>
   
    
</html>