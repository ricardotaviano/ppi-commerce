<?php


function conectar()
{
    $u = 'root';
    $s = '1206';
    $pdo = new PDO("mysql:host=localhost;dbname=ppicommerce", $u, $s);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $pdo;
}

function inserir($dados)
{
    $sql = "INSERT INTO produto (nome, preco, categoria) VALUES (?,?,?)";
    $stmt = conectar()->prepare($sql);
    $stmt->bindValue(1, $dados['nome']);
    $stmt->bindValue(2, $dados['preco']);
    $stmt->bindValue(3, $dados['categoria']);
    $stmt->execute();
}

function buscarnome($name)
{
    $sql = "SELECT * FROM produto WHERE nome LIKE ? ";
    $stmt = conectar()->prepare($sql);
    $stmt->bindValue(1, "%{$name}%");
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_OBJ);
}


function buscar()
{
    $sql = "SELECT * FROM produto";
    $stmt = conectar()->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_OBJ);
}

function buscarPorId($id)
{
    $sql = "SELECT * FROM produto WHERE id = ?";
    $stmt = conectar()->prepare($sql);
    $stmt->bindValue(1, $id);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_OBJ);
}

function atualizar($dados)
{
    $sql = "UPDATE produto SET nome = ?, preco = ?, categoria = ? WHERE id = ?";
    $stmt = conectar()->prepare($sql);
    $stmt->bindValue(1, $dados['nome']);
    $stmt->bindValue(2, $dados['preco']);
    $stmt->bindValue(3, $dados['categoria']);
    $stmt->bindValue(4, $dados['id']);
    $stmt->execute();
}

function excluir($id)
{
    $sql = "DELETE FROM produto WHERE id = ?";
    $stmt = conectar()->prepare($sql);
    $stmt->bindValue(1, $id);
    $stmt->execute();
}
