<!DOCTYPE html >

    
<?php 
require ("conexao.php");
if ( isset($_POST['btnEnviar']) )
{
    $dados['nome'] = $_POST['nome'];
    $dados['preco'] = $_POST['preco'];
    $dados['categoria'] = $_POST['cat'];
    inserir($dados);
    echo "<span class=\"alert alert-success\">Produto Inserido</span>";
}

if ( isset($_POST['btnUpd']) )
{
    $dads['nome'] = $_POST['nome'];
    $dads['preco'] = $_POST['preco'];
    $dads['categoria'] = $_POST['cat'];
    $dads['id'] = $_POST['id'] ;
    atualizar($dads);
    echo "<span class=\"alert alert-success\">Produto Atualizado</span>";
}

if (isset($_POST['del'])) {
    
                  $del = $_POST['id'];
                    
                    excluir($del);

}
             
if (isset($_POST['edit'])) {
    
    $bus = $_POST['id'];

    $dads = buscarPorId($bus) ;
    
foreach ($dads as $d) {

    $nm = $d->nome;
    $pc = $d->preco;
    $ct = $d->categoria;
    $id = $d->id;
}

}


?>


<html>
  
    <?php require 'head.php'; ?>

    
    
    
    <body>
    
    <div class="container-fluid telaadmin" > 
    
        <center> <h3 class="align-middle" style="padding-top: 30px " > Página do Administrador </h3>  </center>
 
    
    </div>
    
        
        <div class="container-fluid" > 
        
        <div class="row" >
           
            <div class="col-xl-2 col-lg-2 col-md-2 hidden-sm hidden-xs"></div>
            
            
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-6 cxopcao border" >
  
                <p class="text-center" > <h3> CADASTRAR </h3> </p>

              <center> <form method="post" action="" >
           
            
            <div class="form-row">
              <div class="form-group col-12"> 
               
               <label> Nome  </label>
               <input type="text" class="form-control" placeholder="Insira o nome do produto"  name="nome" required>
           
           </div>
            
           </div>
 
            <div class="form-row">
              <div class="form-group col-12"> 
               
               <label> Preço </label>
               <input type="number" class="form-control" step="0.01" min="0" placeholder="Preço do produto" name="preco" required>
           
           </div>
            
           </div>
            
              <div class="form-row">
              <div class="form-group col-12"> 
               
               <label> Categoria </label>
               <select class="form-control" id="exampleFormControlSelect1" name="cat" required>
                   <option value="" ></option>
                   <option value="1" >Roteador</option>
               <option value="2" >Antena</option>
               <option value="3"> Routerboard </option>
              
            </select>
              
               <br>
               
             <button type="submit" class="btn btn-primary " name="btnEnviar"> Cadastrar </button>  
           
           </div>
            
           </div>
            
            
        </form> </center>
            

            </div>
                    
           <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-6 cxopcao border" >
  
                <p class="text-center" > <h3> EDITAR </h3> </p>
          
            <center>
                    
                    <form method="post" action="" >
           
            
            <div class="form-row">
              <div class="form-group col-12"> 
               
               <label> Nome  </label>
               <input type="text" value="<?php if(!is_null($nm)){ echo @$nm; }else{ echo @$nm ;} ?>" class="form-control" placeholder="Insira o nome do produto"  name="nome" required>
           
           </div>
            
           </div>
 
            <div class="form-row">
              <div class="form-group col-12"> 
               
               <label> Preço </label>
               <input type="number" value="<?php if(!is_null($pc)){ echo $pc ; } ?>" class="form-control" step="0.01" min="0" placeholder="Preço do produto" name="preco" required>
           
           </div>
            
           </div>
            
              <div class="form-row">
              <div class="form-group col-12"> 
               
               <label> Categoria </label>
               <select class="form-control" values="<?php if(!is_null($ct)) { echo $ct ; } ?>" id="exampleFormControlSelect1" name="cat" required>
                   <option value="1" >Roteador</option>
               <option value="2" >Antena</option>
               <option value="3"> Routerboard </option>
              
               
               <input type="hidden" value ="<?php echo $id ?>" name="id" required>
               
            </select>
              
               <br>
               
             <button type="submit" class="btn btn-primary " name="btnUpd"> Editar </button>  
           
           </div>
            
           </div>
            
            
        </form>
                   

                </center>
            
            </div>

            
        </div> 
            
            
         <div class="col-xl-2 col-lg-2 col-md-2 hidden-sm hidden-xs"></div>   
            
            
            
        </div>  
 
        <div >
    
        <?php 
        
        $prods = buscar();
        
        ?>
        
        
        <table class="table">
            
            <thead>
                
                <tr>
                    
                    <th>
                        
                        Nome 
                        
                        
                    </th>
                    
                     <th>
                        
                        Preco
                        
                        
                    </th>
                    
                     <th>
                        
                        Categoria 
                        
                        
                    </th>
                    
                </tr>
                
                
            </thead>
            
            <tbody >
            
                <?php foreach ( $prods as $item ){ ?>
                
                <tr>
                    
                    
                    <td><?php echo $item->nome  ?></td>
                    <td><?php echo $item->preco  ?></td>
                    <td><?php if ( $item->categoria == 1 ) { echo "Roteador"; }else if ($item->categoria == 2 ) { echo "Antena"; } else { echo "Routerboard"; }?></td>
                    
            <form method="POST" action="" > 
                    
                    <td> 
                        
                        <input type="hidden" value ="<?php echo $item->id ?>" name="id" required>
                        <button class="btn btn-primary " type="submit" name="edit" value="<?php echo $item->id  ?> " > Editar </button> 
                        <button class="btn btn-danger " type="submit" name="del" value="<?php echo $item->id  ?> " > Excluir </button> </td>
                    
              </form>    
                      
                </tr>
                
                <?php } ?>
                
            </tbody>
            
            
            
        </table>
        
        
    </div>
    
    
    <a href="index.php">Voltar</a>
        
            
    </body>
    
    
</html>